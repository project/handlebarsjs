#Handlebars.js

This module is helper for rendering [Handlebars.js](https://handlebarsjs.com/) templates in Twig.

##Requirements
[xamin/handlebars.php](https://github.com/XaminProject/handlebars.php) library

```
composer require "xamin/handlebars.php"
```

##Installation and usage

###Installation

It is required to install `xamin/handlebars.php` first, as described in requirements.

Handlebars.js module can be installed in two ways:
* Download and enable module in [standard way](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules)
* Download module with composer and enable in standard way:
```
composer require "drupal/handlebarsjs"
```

If composer is used, requirements will be downloaded automatically.

###Usage

We have template **block-foo.hbs** with content:
```
<div data-component="block-foo">
    <h2>{{title}}</h2>
    {{{description}}}
</div>
```

We define new theme with name **block_foo**:
```
function mymodule_theme() {
    return [
         'book_foo' => [
            'variables' => [
                'title' => NULL,
                'description' => NULL
            ],
         ],
    ];
}
```

To be able to render that template in Drupal, we should include it in twig file **block-foo.html.twig**:
```
{{ handlebars('block-foo.hbs') }}
```

Let's render template in Drupal:
```
return [
    '#theme' => 'block_foo',
    '#title' => 'Lorem Ipsum'
    '#description' => '<p>Foo Bar</p>',
];
```
Render result should be:

```
<div data-component="block-foo">
    <h2>Lorem Ipsum?</h2>
    <p>Foo Bar</p>
</div>
```
